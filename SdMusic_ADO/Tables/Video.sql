﻿CREATE TABLE [dbo].[Video]
(
	[Id] INT NOT NULL, 
    [SrcLink] VARCHAR(250) NULL, 
    [Title] VARCHAR(50) NULL, 
    CONSTRAINT [PK_Video] PRIMARY KEY ([Id]) 
)
