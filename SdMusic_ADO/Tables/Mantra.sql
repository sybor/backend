﻿CREATE TABLE [dbo].[Mantra]
(
	[Id] INT NOT NULL IDENTITY, 
	[Title] VARCHAR(100),
	[Lyrics] VARCHAR(250),
	[Comment] VARCHAR(250)
    CONSTRAINT [PK_Mantra] PRIMARY KEY ([Id]) 
)
