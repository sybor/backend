﻿CREATE TABLE [dbo].[Contact]
(
	[Id] INT NOT NULL, 
    [Nom] NVARCHAR(250) NULL, 
    [Email] NCHAR(100) NULL, 
    [Message] NCHAR(250) NULL, 
    CONSTRAINT [PK_Contact] PRIMARY KEY ([Id]) 
)
