﻿CREATE TABLE [dbo].[News]
(
	[Id] INT NOT NULL IDENTITY,
	[Title] VARCHAR(100),
	[Subtitle] VARCHAR(100),
	[Date] DATETIME2,
	[ImageUrl] VARCHAR(250),
	[Text] NTEXT,
	[Link] VARCHAR(250),
    CONSTRAINT [PK_News] PRIMARY KEY ([Id])
)
