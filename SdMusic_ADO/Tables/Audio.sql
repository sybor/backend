﻿CREATE TABLE [dbo].[Audio]
(
	[Id] INT NOT NULL, 
    [SrcLink] VARCHAR(250) NULL, 
    [Title] VARCHAR(50) NULL
    CONSTRAINT [PK_Audio] PRIMARY KEY ([Id]) 
)
