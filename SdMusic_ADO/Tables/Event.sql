﻿CREATE TABLE [dbo].[Event]
(
	[Id] INT NOT NULL IDENTITY, 
	[Title] VARCHAR(100),
	[Date] DATETIME2,
	[Text] NTEXT
    CONSTRAINT [PK_Event] PRIMARY KEY ([Id]), 
    
)
