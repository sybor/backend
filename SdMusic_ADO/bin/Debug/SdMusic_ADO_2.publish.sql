﻿/*
Script de déploiement pour Sdmusic

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "Sdmusic"
:setvar DefaultFilePrefix "Sdmusic"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
Modèle de script de post-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront ajoutées au script de compilation.		
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de post-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de post-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


Insert into News values (1,'Première chanson avec Astral Journey',
                         'Remember: souviens-toi de ton infinité',
                         '2019-09-15',
                         '../../assets/img/remember.png',
                         'Rappelons-nous à quel point nous sommes puissants en tant qu''êtres humains. \
                         Depuis tout petit nous apprenons à nous conformer à la société mais \
                         nous sommes nés avec un génie et une créativité illimitée. \
                         Il est temps maintenant de s''en souvenir...',
                         'https://www.youtube.com/watch?v=bvf5IhA5xmE'),

                         (2,'Clip avec Romane Collin',
                         'Blue Lights, une reprise de Jorja Smith',
                         '2019-10-09',
                         '../../assets/img/romane-collin.jpg',
                         'La chanteuse belge Romane Collin vient de sortir son dernier clip live. \
                          Elle est accompagnée par Sébastien à la batterie et Kevin Jaucot aux claviers \
                          et à la production. \
                          <p>Regarder le clip sur <a href="https://youtu.be/9dEQv8couHA">Youtube</a>.</p>',
                         'https://www.youtube.com/watch?v=O_-x3UhZ08s'),

                         (3,'Nouveau studio Live "Seven"',
                         'Regarde notre premier studio Live',
                         '2019-10-30',
                         '../../assets/img/kirtan-seven.jpg',
                         'Regarder le clip sur <a href="https://youtu.be/9dEQv8couHA">Youtube</a>.',
                         'https://youtu.be/9dEQv8couHA'),

                         (4,'Sortie du premier single d''Astral Journey ',
                         'Un lointain poème des légendes indiennes',
                         '2019-12-16',
                         '../../assets/img/excelsior-cover.jpg',
                         '<p>La chanson s''inspire de la légende du roi Ravanna qui traversa  \
                        toutes les rizières et montagnes, \
                        depuis le Sri-Lanka jusqu''au nord de l''Inde.</p> \
                        <p>Excelsior est un hymne d''encouragement et d''espoir pour tous les hommes et les femmes \
                        afin de les guider sur leur chemin dans la grâce et la beauté.</p> \
                        <p>Jata bhujan gapingala sphuratphanamaniprabha<br> \
                        <i>May I find wonderful pleasure in Lord Shiva, who is the advocate of all life</i></p> \
                        <p>Kadambakunkuma dravapralipta digvadhumukhe<br> \
                        <i>With his creeping snake with its reddish brown hood and the shine of its gem on it</i></p> \
                        <p>Madandha sindhu rasphuratvagutariyamedure<br> \
                        <i>Spreading variegated colors on the beautiful faces of the Goddesses of the Directions</i></p> \
                        <p>Mano vinodamadbhutam bibhartu bhutabhartari<br> \
                        <i>Which is covered by a shimmering shawl made from the skin of a huge, inebriated elephant</i></p> \
                        <p>Ecouter la musique sur <a href="https://music.apple.com/be/album/excelsior/1491189522?i=1491189524&l=fr">iTunes</a> \
                        et <a href="https://open.spotify.com/track/4gjsHO7dGbq434uhFAJOwt">Spotify</a>.</p>',
                         'https://youtu.be/k6o1v4Hztek'),

                         (5,'Sortie de Faith, le deuxième single d''Astral Journey',
                         'Un mantra pour retrouver son amour perdu',
                         '2020-04-06',
                         '../../assets/img/faith.jpg',
                         'Faith est un appel à l''homme afin de se réconcilier avec sa joie et son âme. \
                        La nature et les animaux sont un exemple de comment prendre soin les uns des autres.</p> \
                        <p>Ecouter la chanson sur <a href="https://music.apple.com/be/album/faith/1506179264?i=1506179265&l=fr">iTunes</a> \
                        et <a href="https://open.spotify.com/track/0Le6u965Mg27T43OMupoEC?si=nSkvO4zeT4CJkZI6Wl-feQ">Spotify</a>. \
                        Regarder le clip sur <a href="https://youtu.be/OUSHTd2AjFg">Youtube</a>.',
                         'https://youtu.be/OUSHTd2AjFg'),

                         (6,'Sortie du nouveau single d''Astral Journey',
                         ' avec Timour Montil au chant',
                         '2020-06-01',
                         '../../assets/img/couverture-peuple.jpg',
                         '"Le peuple crie" est une collaboration avec Timour Montil du groupe belge de chanson française "Gueules de Loup". \
                         La chanson est un message pour rester uni en cette période particulière.</p> \
                         <p>Ecouter la chanson sur <a href="https://music.apple.com/be/album/le-peuple-crie-feat-timour-montil-single/1515823864?l=fr">iTunes</a> \
                         et <a href="https://open.spotify.com/track/30gvb4s5yszWr5XJhEMinB?si=i91iQ9POTiKpS2TKuy9pgA">Spotify</a>.',
                         'https://open.spotify.com/track/30gvb4s5yszWr5XJhEMinB?si=i91iQ9POTiKpS2TKuy9pgA');
/*
Insert into Mantra values (1,'SAT SIRI AKAL',
                            'Sat Siri Siri Akal Siri Akal Maha Akal Maha Akal Sat Nam Akal Mourt Ouahé Gourou',
                            'Ce mantra nous aide à établir notre être au-delà des changements du temps. Nous sommes éternels et tout ce que nous faisons\
    vient de notre âme.'),

                            (2,'RA MA DA SA',
                            'Ra Ma Da Sa Sa Sé So Hang',
                            'Mantra de guérison, il harmonise le soi et apporte l''équilibre. \
                            Les huits syllabes stimulennt l''énergie de la colonne vertébrale \
            dans un but de guérison. Ce mantra combine la Terre (Ra Ma Da) et l''éther (Sa Se So Hang).'),

                            (3,'MOUL MANTRA',
                            'Ek Ong Kaar, Sat Nam, Kartaa Pourkh, Nirbhao, Nirvèr, Akâl Mourt, Adjouni, Sèbhang, Gour Presad, Djap ! \
    Aad Satch, Djougaad Satch, Hébi Satch, Naanak Hossi Bhi Satch',
                            'Ce mantra est une boussole pour notre âme, \
    il forme la fondation de la conscience que nous voudrions développer et qui réside au plus profond de notre âme.'),

                            (4,'HAR HAR HAR HAR GOBINDE',
                            'Har Har Har Har Gobindé, Har Har Har Har Moukhandé, Har Har Har Har Oudaré, Har Har Har Har Aparé, \
    Har Har Har Har Hariang, Har Har Har Har Kariang, Har Har Har Har Nirnamé, Har Har Har Har Akamé',
                            'Relie le mental à la prospérité et au pouvoir. Har, la force originelle de la création, est répété 4 fois afin de donner du pouvoir.\
    La peur est transformée en détermination et accroit la réserve d''énergie.'),

                            (5,'GOBINDE MUKANDE',
                            'Gobindé Moukhandé Oudaré Aparé, Hariang Kariang, Nirnamé Akamé',
                            'Elimine les blocages karmiques et les erreurs du passé, purifie le champ magnétique et favorise la relaxation.'),

                            (6,'ADI SHAKTI',
                            'Aadi shakti namo namo Sarabe shakti namo namo Pritham bhagvati namo namo Kundalini mata shakti namo namo',
                            'Mantra dévotionnel pour invoquer la puissance créatrice primordiale. \
    C''est une aide pour se libérer des insécurités qui entravent la liberté d''agir.'),

                            (7,'ADES TISE ADES',
                            'Aad anil anaad anaahat, djoug djoug èko vès',
                            'C''est une salutation pour l''infini, permettant de faire venir à soi la connaissance de l''univers.');
    
Insert into Event values (1,'Première chanson d''Astral Journey disponible en ligne',
                            '2019-09-15',
                            'Ecouter la chanson sur <a href="https://www.youtube.com/watch?v=bvf5IhA5xmE">Youtube</a>.'),

                            (2,'Sortie du clip de Romane Collin, "Blue lights"',
                            '2019-10-09',
                            'Regarder le clip sur <a href="https://www.youtube.com/watch?v=O_-x3UhZ08s">Youtube</a>.'),

                            (3,'Mise en ligne de "Seven", le premier clip live d''Astral Journey',
                            '2019-10-30',
                            'Regarder le clip sur <a href="https://youtu.be/9dEQv8couHA">Youtube</a>.'),

                            (4,'Sortie de "Excelsior", le premier single d''Astral Journey',
                            '2019-12-16',
                            'Ecouter la musique sur <a href="https://music.apple.com/be/album/excelsior/1491189522?i=1491189524&l=fr">iTunes</a> \
                            et <a href="https://open.spotify.com/track/4gjsHO7dGbq434uhFAJOwt">Spotify</a>. \
                            Regarder le clip sur <a href="https://youtu.be/k6o1v4Hztek">Youtube</a>.'),

                            (5,'Sortie de "Faith", le deuxième single d''Astral Journey',
                            '2020-04-06',
                            'Ecouter la chanson sur <a href="https://music.apple.com/be/album/faith/1506179264?i=1506179265&l=fr">iTunes</a> \
                            et <a href="https://open.spotify.com/track/0Le6u965Mg27T43OMupoEC?si=nSkvO4zeT4CJkZI6Wl-feQ">Spotify</a>. \
                            Regarder le clip sur <a href="https://youtu.be/OUSHTd2AjFg">Youtube</a>.'),

                            (6,'Sortie du nouveau single d''Astral Journey et Timour Montil',
                            '2020-06-01',
                            'Ecouter la chanson sur <a href="https://music.apple.com/be/album/le-peuple-crie-feat-timour-montil-single/1515823864?l=fr">iTunes</a> \
                            et <a href="https://open.spotify.com/track/30gvb4s5yszWr5XJhEMinB?si=i91iQ9POTiKpS2TKuy9pgA">Spotify</a>.'),

                            (7,'Facebook live avec Sébastien et Gueules de loup en direct de la Cellule 133 à Bruxelles',
                            '2020-06-21',
                            'Regarder sur <a href="https://youtu.be/zGJ6gs2gEEA">Youtube</a>.');*/
GO

GO
PRINT N'Mise à jour terminée.';


GO
