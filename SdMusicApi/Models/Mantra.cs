﻿namespace SdMusicApi.Models
{
    public class Mantra
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Lyrics { get; set; }
        public string Comment { get; set; }
    }
}
