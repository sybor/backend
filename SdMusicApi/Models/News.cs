﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SdMusicApi.Models
{
    public class News
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Date { get; set; }
        public string  ImageUrl { get; set; }
        public string Text { get; set; }
        public string Link { get; set; }
    }
}
