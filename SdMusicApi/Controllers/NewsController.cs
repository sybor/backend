﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SdMusicApi.Models;
using SdMusicApi.Services;

namespace SdMusicApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private NewsService _newsService;

        public NewsController()
        {
            _newsService = new NewsService();
        }

        [HttpGet]
        public ActionResult<IEnumerable <News>> GetAll()
        {
            IEnumerable<News> news = _newsService.GetAll();
            return news.ToList();
        }
    }
}
