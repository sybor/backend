﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SdMusicApi.Models;
using SdMusicApi.Services;

namespace SdMusicApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private EventService _eventService;
        public EventController()
        {
            _eventService = new EventService();
        }

        [HttpGet]
        public ActionResult<IEnumerable<Event>> GetAll()
        {
            IEnumerable<Event> events = _eventService.GetAll();
            return events.ToList();
        }
    }
}
