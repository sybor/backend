﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SdMusicApi.Models;
using SdMusicApi.Services;

namespace SdMusicApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MantraController : ControllerBase
    {
        private MantraService _mantraService;
        public MantraController()
        {
            _mantraService = new MantraService();
        }

        [HttpGet]
        public ActionResult<IEnumerable<Mantra>> GetAll()
        {
            IEnumerable<Mantra> mantras = _mantraService.GetAll();
            return mantras.ToList();
        }
    }
}
