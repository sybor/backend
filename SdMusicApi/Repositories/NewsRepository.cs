﻿using SdMusicApi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SdMusicApi.Repositories
{
    public class NewsRepository
    {
        private string _connectionString;

        private SqlConnection _connection;

        public NewsRepository()
        {
            _connectionString = @"Data Source=DESKTOP-9PPUQUT\SQLEXPRESS;Initial Catalog=Sdmusic;Integrated Security=True;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            _connection = new SqlConnection(_connectionString);
        }
        public int Delete(int id)
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "DELETE FROM News WHERE Id = @param";
                command.CommandText = sql;
                command.Parameters.AddWithValue("param", id);
                _connection.Open();
                return command.ExecuteNonQuery();
            }
        }

        public IEnumerable<News> FindByTitle(string name)
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "SELECT * FROM News WHERE Title = @param";
                command.CommandText = sql;
                command.Parameters.AddWithValue("param", name);
                _connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return new News
                        {
                            Id = (int)reader["Id"],
                            Title = reader["Title"].ToString(),
                            Subtitle = reader["Subtitle"].ToString(),
                            Date = reader["Date"].ToString(),
                            ImageUrl = reader["ImageUrl"].ToString(),
                            Text = reader["Text"].ToString(),
                            Link = reader["Link"].ToString()
                        };
                    }
                }
            }
        }

        public IEnumerable<News> GetAll()
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "SELECT * FROM News order by Date desc";
                command.CommandText = sql;
                _connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return new News
                        {
                            Id = (int)reader["Id"],
                            Title = reader["Title"].ToString(),
                            Subtitle = reader["Subtitle"].ToString(),
                            Date = reader["Date"].ToString(),
                            ImageUrl = reader["ImageUrl"].ToString(),
                            Text = reader["Text"].ToString(),
                            Link = reader["Link"].ToString()
                        };
                    }
                }
            }
        }

        public News GetById(int id)
        {
            News news = new News();
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "SELECT * FROM News WHERE Id = @id";
                command.CommandText = sql;
                command.Parameters.AddWithValue("id", id);
                _connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        news.Id = (int)reader["Id"];
                        news.Title = reader["Title"].ToString();
                        news.Subtitle = reader["Subtitle"].ToString();
                        news.Date = reader["Date"].ToString();
                        news.ImageUrl = reader["ImageUrl"].ToString();
                        news.Text = reader["Text"].ToString();
                        news.Link = reader["Link"].ToString();
                    }
                }
                _connection.Close();
                return news;
            }
        }

        public int Save(News entity)
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "INSERT INTO Contact VALUES(@title, @subtitle, @date, @imageUrl, @text, @link)";
                command.CommandText = sql;
                command.Parameters.AddWithValue("title", entity.Title);
                command.Parameters.AddWithValue("subtitle", entity.Subtitle);
                command.Parameters.AddWithValue("date", entity.Date);
                command.Parameters.AddWithValue("imageUrl", entity.ImageUrl);
                command.Parameters.AddWithValue("text", entity.Text);
                command.Parameters.AddWithValue("link", entity.Link);
                _connection.Open();
                return command.ExecuteNonQuery();
            }
        }

        public int Update(News entity)
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "UPDATE News SET Title = @title, Subtitle = @subtitle, " +
                    "Date = @date, ImageUrl = @imageUrl, Text = @text, Link = @link WHERE Id = @id";
                command.CommandText = sql;
                command.Parameters.AddWithValue("title", entity.Title);
                command.Parameters.AddWithValue("subtitle", entity.Subtitle);
                command.Parameters.AddWithValue("date", entity.Date);
                command.Parameters.AddWithValue("imageUrl", entity.ImageUrl);
                command.Parameters.AddWithValue("text", entity.Text);
                command.Parameters.AddWithValue("link", entity.Link);
                _connection.Open();
                return command.ExecuteNonQuery();
            }
        }
    }
}
