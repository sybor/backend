﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using SdMusicApi.Models;

namespace SdMusicApi.Repositories
{
    public class EventRepository
    {
        private string _connectionString;

        private SqlConnection _connection;

        public EventRepository()
        {
            _connectionString = @"Data Source=DESKTOP-9PPUQUT\SQLEXPRESS;Initial Catalog=Sdmusic;Integrated Security=True;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            _connection = new SqlConnection(_connectionString);
        }
        public int Delete(int id)
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "DELETE FROM Event WHERE Id = @param";
                command.CommandText = sql;
                command.Parameters.AddWithValue("param", id);
                _connection.Open();
                return command.ExecuteNonQuery();
            }
        }

        public IEnumerable<Event> FindByTitle(string name)
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "SELECT * FROM Event WHERE Title = @param";
                command.CommandText = sql;
                command.Parameters.AddWithValue("param", name);
                _connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return new Event
                        {
                            Id = (int)reader["Id"],
                            Title = reader["Title"].ToString(),
                            Date = reader["Date"].ToString(),
                            Text = reader["Text"].ToString()
                        };
                    }
                }
            }
        }

        public IEnumerable<Event> GetAll()
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "SELECT * FROM Event order by Date desc";
                command.CommandText = sql;
                _connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return new Event
                        {
                            Id = (int)reader["Id"],
                            Title = reader["Title"].ToString(),
                            Date = reader["Date"].ToString(),
                            Text = reader["Text"].ToString()
                        };
                    }
                }
            }
        }

        public Event GetById(int id)
        {
            Event ev = new Event();
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "SELECT * FROM Event WHERE Id = @id";
                command.CommandText = sql;
                command.Parameters.AddWithValue("id", id);
                _connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ev.Id = (int)reader["Id"];
                        ev.Title = reader["Title"].ToString();
                        ev.Date = reader["Date"].ToString();
                        ev.Text = reader["Text"].ToString();
                    }
                }
                _connection.Close();
                return ev;
            }
        }

        public int Save(Event entity)
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "INSERT INTO Event VALUES(@title, @date, @text)";
                command.CommandText = sql;
                command.Parameters.AddWithValue("title", entity.Title);
                command.Parameters.AddWithValue("date", entity.Date);
                command.Parameters.AddWithValue("text", entity.Text);
                _connection.Open();
                return command.ExecuteNonQuery();
            }
        }

        public int Update(Event entity)
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "UPDATE Event SET Title = @title, Date = @date, Text = @text WHERE Id = @id";
                command.CommandText = sql;
                command.Parameters.AddWithValue("title", entity.Title);
                command.Parameters.AddWithValue("date", entity.Date);
                command.Parameters.AddWithValue("text", entity.Text);
                _connection.Open();
                return command.ExecuteNonQuery();
            }
        }
    }
}
