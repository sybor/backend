﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using SdMusicApi.Models;

namespace SdMusicApi.Repositories
{
    public class MantraRepository
    {
        private string _connectionString;

        private SqlConnection _connection;

        public MantraRepository()
        {
            _connectionString = @"Data Source=DESKTOP-9PPUQUT\SQLEXPRESS;Initial Catalog=Sdmusic;Integrated Security=True;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            _connection = new SqlConnection(_connectionString);
        }
        public int Delete(int id)
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "DELETE FROM Mantra WHERE Id = @param";
                command.CommandText = sql;
                command.Parameters.AddWithValue("param", id);
                _connection.Open();
                return command.ExecuteNonQuery();
            }
        }

        public IEnumerable<Mantra> FindByTitle(string name)
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "SELECT * FROM Mantra WHERE Title = @param";
                command.CommandText = sql;
                command.Parameters.AddWithValue("param", name);
                _connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return new Mantra
                        {
                            Id = (int)reader["Id"],
                            Title = reader["Title"].ToString(),
                            Lyrics = reader["Lyrics"].ToString(),
                            Comment = reader["Comment"].ToString()
                        };
                    }
                }
            }
        }

        public IEnumerable<Mantra> GetAll()
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "SELECT * FROM Mantra";
                command.CommandText = sql;
                _connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return new Mantra
                        {
                            Id = (int)reader["Id"],
                            Title = reader["Title"].ToString(),
                            Lyrics = reader["Lyrics"].ToString(),
                            Comment = reader["Comment"].ToString()
                        };
                    }
                }
            }
        }

        public Mantra GetById(int id)
        {
            Mantra mantra = new Mantra();
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "SELECT * FROM Mantra WHERE Id = @id";
                command.CommandText = sql;
                command.Parameters.AddWithValue("id", id);
                _connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        mantra.Id = (int)reader["Id"];
                        mantra.Title = reader["Title"].ToString();
                        mantra.Lyrics = reader["Lyrics"].ToString();
                        mantra.Comment = reader["Comment"].ToString();
                    }
                }
                _connection.Close();
                return mantra;
            }
        }

        public int Save(Mantra entity)
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "INSERT INTO Mantra VALUES(@title, @lyrics, @comment)";
                command.CommandText = sql;
                command.Parameters.AddWithValue("title", entity.Title);
                command.Parameters.AddWithValue("date", entity.Lyrics);
                command.Parameters.AddWithValue("text", entity.Comment);
                _connection.Open();
                return command.ExecuteNonQuery();
            }
        }

        public int Update(Mantra entity)
        {
            using (SqlCommand command = _connection.CreateCommand())
            {
                string sql = "UPDATE Mantra SET Title = @title, Lyrics = @lyrics, Comment = @comment WHERE Id = @id";
                command.CommandText = sql;
                command.Parameters.AddWithValue("title", entity.Title);
                command.Parameters.AddWithValue("date", entity.Lyrics);
                command.Parameters.AddWithValue("text", entity.Comment);
                _connection.Open();
                return command.ExecuteNonQuery();
            }
        }
    }
}
