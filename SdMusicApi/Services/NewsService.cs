﻿using SdMusicApi.Models;
using SdMusicApi.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SdMusicApi.Services
{
    public class NewsService
    {
        private NewsRepository _newsRepository;

        public NewsService()
        {
            _newsRepository = new NewsRepository();
        }

        public int Delete(int id)
        {
            return _newsRepository.Delete(id);
        }

        public IEnumerable<News> FindByTitle(string title)
        {
            IEnumerable<News> news = _newsRepository.FindByTitle(title).Select(x => x).ToList();
            return news;
        }

        public IEnumerable<News> GetAll()
        {
            return _newsRepository.GetAll().Select(x => x);
        }

        public News GetById(int id)
        {
            return _newsRepository.GetById(id);
        }

        public int Save(News entity)
        {
            return _newsRepository.Save(entity);
        }

        public int Update(News entity)
        {
            return _newsRepository.Update(entity);
        }
    }
}
