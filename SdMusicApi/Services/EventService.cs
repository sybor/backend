﻿using SdMusicApi.Models;
using SdMusicApi.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SdMusicApi.Services
{
    public class EventService
    {
        private EventRepository _eventRepository;
        public EventService()
        {
            _eventRepository = new EventRepository();
        }

        public int Delete(int id)
        {
            return _eventRepository.Delete(id);
        }

        public IEnumerable<Event> FindByTitle(string title)
        {
            IEnumerable<Event> news = _eventRepository.FindByTitle(title).Select(x => x).ToList();
            return news;
        }

        public IEnumerable<Event> GetAll()
        {
            return _eventRepository.GetAll().Select(x => x);
        }

        public Event GetById(int id)
        {
            return _eventRepository.GetById(id);
        }

        public int Save(Event entity)
        {
            return _eventRepository.Save(entity);
        }

        public int Update(Event entity)
        {
            return _eventRepository.Update(entity);
        }
    }
}
