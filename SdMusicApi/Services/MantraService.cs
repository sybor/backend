﻿using SdMusicApi.Models;
using SdMusicApi.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SdMusicApi.Services
{
    public class MantraService
    {
        private MantraRepository _mantraRepository;
        public MantraService()
        {
            _mantraRepository = new MantraRepository();
        }

        public int Delete(int id)
        {
            return _mantraRepository.Delete(id);
        }

        public IEnumerable<Mantra> FindByTitle(string title)
        {
            IEnumerable<Mantra> mantra = _mantraRepository.FindByTitle(title).Select(x => x).ToList();
            return mantra;
        }

        public IEnumerable<Mantra> GetAll()
        {
            return _mantraRepository.GetAll().Select(x => x);
        }

        public Mantra GetById(int id)
        {
            return _mantraRepository.GetById(id);
        }

        public int Save(Mantra entity)
        {
            return _mantraRepository.Save(entity);
        }

        public int Update(Mantra entity)
        {
            return _mantraRepository.Update(entity);
        }
    }
}
